import 'package:app_test4/bloc/bloc_export.dart';

class MainBloc {
  static List<BlocProvider> allBlocs = [
    BlocProvider<TaskBloc>(create: (context) => TaskBloc()),
    BlocProvider<DarkThemeCubit>(create: (context) => DarkThemeCubit())
  ];
}