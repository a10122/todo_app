import 'package:app_test4/bloc/bloc_export.dart';

class DarkThemeCubit extends HydratedCubit<bool>{
  DarkThemeCubit() : super(false);

  void toggleDarkTheme({required bool value}) {
    emit(value);
  }

  @override
  bool? fromJson(Map<String, dynamic> json) {
    return json['isDark'];
  }

  @override
  Map<String, dynamic>? toJson(bool state) {
    return {'isDark': state};
  }
}