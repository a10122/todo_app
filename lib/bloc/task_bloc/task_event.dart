import 'package:equatable/equatable.dart';

import '../../models/task_model.dart';

abstract class TaskEvent extends Equatable {
  const TaskEvent();

  @override
  // TODO: implement props
  List<Object?> get props => [];
}

class AddedTask extends TaskEvent {
  final Task task;
  const AddedTask({required this.task});

  @override
  // TODO: implement props
  List<Object?> get props => [task];
}

class UpdatedTask extends TaskEvent {
  final Task task;
  const UpdatedTask({required this.task});

  @override
  // TODO: implement props
  List<Object?> get props => [task];
}

class RemovedTask extends TaskEvent {
  final Task task;
  const RemovedTask({required this.task});

  @override
  // TODO: implement props
  List<Object?> get props => [task];
}

class DeletedTask extends TaskEvent {
  final Task task;
  const DeletedTask({required this.task});

  @override
  // TODO: implement props
  List<Object?> get props => [task];
}