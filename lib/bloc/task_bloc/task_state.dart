import 'package:equatable/equatable.dart';

import '../../models/task_model.dart';

class TaskState extends Equatable {
  final List<Task> pendingTasks;
  final List<Task> doneTasks;
  final List<Task> favouriteTasks;

  final List<Task> removedTasks;

  const TaskState(
      {this.pendingTasks = const <Task>[],
      this.doneTasks = const <Task>[],
      this.favouriteTasks = const <Task>[],
      this.removedTasks = const <Task>[]});

  @override
  // TODO: implement props
  List<Object?> get props =>
      [pendingTasks, doneTasks, favouriteTasks, removedTasks];

  Map<String, dynamic> toMap() {
    return {
      'pendingTasks': pendingTasks.map((e) => e.toMap()).toList(),
      'doneTasks': doneTasks.map((e) => e.toMap()).toList(),
      'favouriteTasks': favouriteTasks.map((e) => e.toMap()).toList(),
      'removedTasks': removedTasks.map((e) => e.toMap()).toList(),
    };
  }

  factory TaskState.fromMap(Map<String, dynamic> map) {
    return TaskState(
      pendingTasks: List.from(map['pendingTasks'].map((e) => Task.fromMap(e))),
      doneTasks: List.from(map['doneTasks'].map((e) => Task.fromMap(e))),
      favouriteTasks: List.from(map['favouriteTasks'].map((e) => Task.fromMap(e))),
      removedTasks: List.from(map['removedTasks'].map((e) => Task.fromMap(e))),
    );
  }
}
