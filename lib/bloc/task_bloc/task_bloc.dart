import 'package:app_test4/bloc/task_bloc/task_event.dart';
import 'package:app_test4/bloc/task_bloc/task_state.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';
import '../../models/task_model.dart';

class TaskBloc extends HydratedBloc<TaskEvent, TaskState> {
  TaskBloc() : super(const TaskState()) {
    on<AddedTask>(_onAddedTask);
    on<UpdatedTask>(_onUpdatedTask);
    on<RemovedTask>(_onRemovedTask);
    on<DeletedTask>(_onDeletedTask);
  }

  void _onAddedTask(AddedTask event, Emitter<TaskState> emit) {
    final state = this.state;
    emit(
      TaskState(
          pendingTasks: List.from(state.pendingTasks)..add(event.task),
          doneTasks: state.doneTasks,
          favouriteTasks: state.favouriteTasks,
          removedTasks: state.removedTasks),
    );
  }

  void _onUpdatedTask(UpdatedTask event, Emitter<TaskState> emit) {
    final state = this.state;
    final task = event.task;
    List<Task> pendingTasks = state.pendingTasks;
    List<Task> doneTasks = state.doneTasks;
    task.isDone == false
        ? {
            pendingTasks = List.from(pendingTasks)..remove(task),
            doneTasks = List.from(doneTasks)
              ..insert(0, task.copyWith(isDone: true))
          }
        : {
            doneTasks = List.from(doneTasks)..remove(task),
            pendingTasks = List.from(pendingTasks)
              ..insert(0, task.copyWith(isDone: false))
          };
    emit(
      TaskState(
        pendingTasks: pendingTasks,
        doneTasks: doneTasks,
        favouriteTasks: state.favouriteTasks,
        removedTasks: state.removedTasks,
      ),
    );
  }

  void _onRemovedTask(RemovedTask event, Emitter<TaskState> emit) {
    final state = this.state;
    emit(
      TaskState(
        pendingTasks: List.from(state.pendingTasks)..remove(event.task),
        doneTasks: List.from(state.doneTasks)..remove(event.task),
        favouriteTasks: List.from(state.favouriteTasks)..remove(event.task),
        removedTasks: List.from(state.removedTasks)
          ..add(event.task.copyWith(isDeleted: true)),
      ),
    );
  }

  void _onDeletedTask(DeletedTask event, Emitter<TaskState> emit) {
    final state = this.state;
    emit(TaskState(
        pendingTasks: state.pendingTasks,
        doneTasks: state.doneTasks,
        favouriteTasks: state.favouriteTasks,
        removedTasks: List.from(state.removedTasks)..remove(event.task)));
  }

  @override
  TaskState? fromJson(Map<String, dynamic> json) {
    return TaskState.fromMap(json);
  }

  @override
  Map<String, dynamic>? toJson(TaskState state) {
    return state.toMap();
  }
}
