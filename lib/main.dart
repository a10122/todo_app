import 'package:app_test4/bloc/main_bloc.dart';
import 'package:app_test4/screens/pending_task.dart';
import 'package:app_test4/services/router.dart';
import 'package:app_test4/services/theme.dart';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import 'package:app_test4/bloc/bloc_export.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  HydratedBloc.storage = await HydratedStorage.build(
      storageDirectory: await getApplicationDocumentsDirectory());
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  MyApp({super.key});

  final _appRouter = AppRouter();

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: MainBloc.allBlocs,
      child: BlocBuilder<DarkThemeCubit, bool>(
        builder: (context, state) {
          return _buildMaterialApp(context, state);
        }
      ),
    );
  }

  Widget _buildMaterialApp(BuildContext context, bool state) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      themeMode: state ? ThemeMode.dark : ThemeMode.light,
      theme: AppTheme.lightTheme,
      darkTheme: AppTheme.darkTheme,
      onGenerateRoute: _appRouter.onGenerateRoute,
      // home: const MyHomePage(),
    );
  }
}
