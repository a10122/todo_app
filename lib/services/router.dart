import 'package:flutter/material.dart';

import '../screens/pending_task.dart';
import '../screens/recycle_bin_screen.dart';
import '../screens/tabs_screen.dart';

class AppRouter {
  Route? onGenerateRoute(RouteSettings settings) {
    switch (settings.name) {
      case '/':
        return MaterialPageRoute(
          builder: (context) => TabsScreen(),
        );
        break;
      // case '/myTask':
      //   return MaterialPageRoute(builder: (context) => const MyHomePage());
      //   break;
      case '/recycleBin':
        return MaterialPageRoute(
          builder: (context) => const RecycleBin(),
        );
        break;
      default:
        return null;
    }
  }
}
