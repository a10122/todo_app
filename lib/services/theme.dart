import 'package:flutter/material.dart';

class AppTheme {
  static ThemeData lightTheme = ThemeData(
      colorSchemeSeed: Colors.purple,
      brightness: Brightness.light,
      backgroundColor: Colors.white,
      scaffoldBackgroundColor: Colors.white,
      textTheme: const TextTheme(
          bodyMedium: TextStyle(color: Colors.black, fontSize: 18)),
      fontFamily: 'Poppins');

  static ThemeData darkTheme = ThemeData(
      brightness: Brightness.dark,
      backgroundColor: Colors.black,
      scaffoldBackgroundColor: Colors.grey[700],
      textTheme: const TextTheme(
          bodyMedium: TextStyle(color: Colors.white, fontSize: 18)),
      fontFamily: 'Poppins');
}
