import 'package:app_test4/screens/drawer.dart';
import 'package:app_test4/screens/favourite_task.dart';
import 'package:flutter/material.dart';

import '../bloc/bloc_export.dart';
import 'add_task_modal_bottom.dart';
import 'done_task.dart';
import 'pending_task.dart';

class TabsScreen extends StatefulWidget {
  const TabsScreen({Key? key}) : super(key: key);

  @override
  State<TabsScreen> createState() => _TabsScreenState();
}

class _TabsScreenState extends State<TabsScreen> {
  final List<Map<String, dynamic>> _pageDetails = [
    {'pageName': const PendingTasks(), 'title': 'Pending tasks'},
    {'pageName': const DoneTasks(), 'title': 'Done tasks'},
    {'pageName': const FavouriteTasks(), 'title': 'Favourite tasks'},
  ];

  var _selectedPage = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(_pageDetails[_selectedPage]['title']),
        actions: [
          IconButton(
              onPressed: () => HydratedBloc.storage.clear(),
              icon: const Icon(Icons.clear))
        ],
      ),
      drawer: const MyDrawer(),
      body: _pageDetails[_selectedPage]['pageName'],
      floatingActionButton: _selectedPage == 0 ? FloatingActionButton(
        child: const Icon(Icons.add),
        onPressed: () => _showAddTaskModalBottom(context),
      ) : null,
      bottomNavigationBar: _buildBottomNavBar(),
    );
  }

  Widget _buildBottomNavBar() {
    return BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        selectedIconTheme: IconThemeData(color: Theme.of(context).primaryColor, size: 30),
        showSelectedLabels: true,
        showUnselectedLabels: true,
        currentIndex: _selectedPage,
        onTap: (index) {
          setState(() {
            _selectedPage = index;
          });
        },
        items: const [
          BottomNavigationBarItem(
              icon: Icon(Icons.format_list_bulleted),
              label: 'Pending tasks'),
          BottomNavigationBarItem(
              icon: Icon(Icons.done),
              label: 'Done tasks'),
          BottomNavigationBarItem(
              icon: Icon(Icons.favorite_outline),
              label: 'Favourite tasks'),
        ]);
  }

  void _showAddTaskModalBottom(BuildContext context) {
    showModalBottomSheet(
      isScrollControlled: true, // không bị bàn phím che mất textfield
      context: context,
      builder: (context) {
        return SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.only(
                left: 20,
                right: 20,
                bottom: MediaQuery.of(context).viewInsets.bottom),
            child: const AddTaskModalBottom(),
          ),
        );
      },
    );
  }
}
