import 'package:app_test4/bloc/bloc_export.dart';
import 'package:flutter/material.dart';

import '../bloc/task_bloc/task_state.dart';

class MyDrawer extends StatelessWidget {
  const MyDrawer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Drawer(
        child: Column(
          children: [
            Container(
              padding: const EdgeInsets.symmetric(vertical: 16, horizontal: 20),
              width: double.infinity,
              color: Theme.of(context).primaryColor,
              child: const Text('Task Drawer', style: TextStyle(color: Colors.white, fontSize: 24),),
            ),
            BlocBuilder<TaskBloc, TaskState>(
                builder: (context, state) {
                  return GestureDetector(
                    onTap: () =>
                        Navigator.of(context).pushNamedAndRemoveUntil(
                            '/', (route) => false),
                    child: ListTile(
                      leading: const Icon(Icons.folder_special),
                      title: const Text('My Tasks'),
                      trailing: Text('${state.pendingTasks.length} | ${state.doneTasks.length}'),
                    ),
                  );
                }
            ),
            const Divider(),
            BlocBuilder<TaskBloc, TaskState>(
                builder: (context, state) {
                  return GestureDetector(
                    onTap: () =>
                        Navigator.of(context).pushNamedAndRemoveUntil(
                            '/recycleBin',(route) => false),
                    child: ListTile(
                      leading: const Icon(Icons.delete),
                      title: const Text('Recycle Bin'),
                      trailing: Text('${state.removedTasks.length}'),
                    ),
                  );
                }
            ),
            const Divider(),
            BlocBuilder<DarkThemeCubit, bool>(builder: (context, state) {
              return Padding(
                padding: const EdgeInsets.only(left: 10, right: 10),
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const Text('Dark mode'),
                    Switch(value: state, onChanged: (value) {
                      context.read<DarkThemeCubit>().toggleDarkTheme(
                          value: value);
                    },)
                  ],
                ),
              );
            },)
          ],
        ),
      ),
    );
  }
}
