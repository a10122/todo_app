import 'package:app_test4/bloc/task_bloc/task_event.dart';
import 'package:app_test4/models/task_model.dart';
import 'package:flutter/material.dart';
import 'package:uuid/uuid.dart';
import 'package:app_test4/bloc/bloc_export.dart';

class AddTaskModalBottom extends StatelessWidget {
  const AddTaskModalBottom({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    TextEditingController controller = TextEditingController();
    TextEditingController descriptionController = TextEditingController();
    return Column(
      children: [
        const Text(
          'Add Task',
          style: TextStyle(fontSize: 24),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 10, bottom: 10),
          child: TextField(
            controller: controller,
            autofocus: true,
            decoration: const InputDecoration(
              label: Text('title'),
              border: OutlineInputBorder(),
            ),
          ),
        ),
        TextField(
          controller: descriptionController,
          autofocus: false,
          minLines: 3,
          maxLines: 5,
          decoration: const InputDecoration(
            label: Text('description'),
            border: OutlineInputBorder(),
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            TextButton(
                onPressed: () => Navigator.pop(context),
                child: const Text('Cancel')),
            ElevatedButton(
              onPressed: () {
                final title = controller.text;
                final description = descriptionController.text;
                context.read<TaskBloc>().add(AddedTask(task: Task(id: const Uuid().v1(), title: title, description: description)));
                Navigator.pop(context);
              },
              child: const Text('Add'),
            )
          ],
        )
      ],
    );
  }
}
