import 'package:app_test4/screens/task_tile.dart';
import 'package:flutter/material.dart';
import 'package:app_test4/bloc/bloc_export.dart';
import '../bloc/task_bloc/task_event.dart';
import '../models/task_model.dart';

class TasksListRendered extends StatelessWidget {
  const TasksListRendered({
    super.key,
    required this.allTasks,
  });

  final List<Task> allTasks;

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: SingleChildScrollView(
        child: ExpansionPanelList.radio(
            children: allTasks
                .map((task) => ExpansionPanelRadio(
                      value: task.id,
                      headerBuilder: (context, isExpanded) =>
                          TaskTile(task: task),
                      body: Align(
                        alignment: Alignment.centerLeft,
                        child: Padding(
                          padding: const EdgeInsets.only(left: 5),
                          child: SelectableText.rich(TextSpan(children: [
                            const TextSpan(
                              text: 'Text:  ',
                              style: TextStyle(fontWeight: FontWeight.bold),
                            ),
                            TextSpan(text: '${task.title}\n\n'),
                            const TextSpan(
                              text: 'Description:  ',
                              style: TextStyle(fontWeight: FontWeight.bold),
                            ),
                            TextSpan(text: '${task.description}\n')
                          ])),
                        ),
                      ),
                    ))
                .toList()),
      ),
    );
  }

  void _onDeleteTask(BuildContext context, Task task) {
    task.isDeleted!
        ? context.read<TaskBloc>().add(DeletedTask(task: task))
        : context.read<TaskBloc>().add(RemovedTask(task: task));
  }
}

// return Expanded(
// child: ReorderableListView.builder(
// itemCount: allTasks.length,
// onReorder: (oldIndex, newIndex) {
// final index = newIndex > oldIndex ? newIndex - 1 : newIndex;
// var item = allTasks.removeAt(oldIndex);
// allTasks.insert(index, item);
// },
// itemBuilder: (context, index) {
// var task = allTasks[index];
// return ListTile(
// key: ValueKey(task),
// title: Text(task.title, style: TextStyle(
// decoration: task.isDone! ? TextDecoration.lineThrough : null),),
// trailing: Row(
// mainAxisSize: MainAxisSize.min,
// children: [
// Checkbox(value: task.isDone, onChanged: (value) {
// context.read<TaskBloc>().add(UpdatedTask(task: task));
// }),
// IconButton(onPressed: () {
// _onDeleteTask(context, task);
// }, icon: const Icon(Icons.delete))
// ],
// ),
// );
// },
// ),
// );
