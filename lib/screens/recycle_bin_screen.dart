import 'package:app_test4/bloc/bloc_export.dart';
import 'package:app_test4/screens/task_list_rendered.dart';
import 'package:flutter/material.dart';

import '../bloc/task_bloc/task_state.dart';
import 'drawer.dart';

class RecycleBin extends StatelessWidget {
  const RecycleBin({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<TaskBloc, TaskState>(
      builder: (context, state) {
        return Scaffold(
          appBar: AppBar(
            title: const Text('Recycle Bin'),
            centerTitle: true,
            elevation: 0,
          ),
          drawer: const MyDrawer(),
          body: Column(
            children: [
              Center(
                child: Chip(label: Text(state.removedTasks.length > 1 ? ' ${state.removedTasks.length} tasks' : 'task')),
              ),
              TasksListRendered(allTasks: state.removedTasks)
            ],
          ),
        );
      }
    );
  }
}
