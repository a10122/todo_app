import 'package:app_test4/bloc/task_bloc/task_state.dart';
import 'package:app_test4/screens/task_list_rendered.dart';
import 'package:flutter/material.dart';
import '../models/task_model.dart';
import 'add_task_modal_bottom.dart';
import 'package:app_test4/bloc/bloc_export.dart';


class PendingTasks extends StatelessWidget {
  const PendingTasks({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<TaskBloc, TaskState>(
      builder: (context, state) {
        final List<Task> allTasks = state.pendingTasks;
        return Column(
          children: [
            Center(
              child: Chip(
                label: Text(allTasks.length > 1
                    ? 'Pending  | ${allTasks.length} tasks'
                    : 'Pending  | ${allTasks.length} task'),
              ),
            ),
            TasksListRendered(allTasks: allTasks)
          ],
        );
      },
    );
  }

}
