import 'package:flutter/material.dart';
import 'package:app_test4/bloc/bloc_export.dart';
import '../bloc/task_bloc/task_event.dart';
import '../models/task_model.dart';

class TaskTile extends StatelessWidget {
  const TaskTile({Key? key, required this.task}) : super(key: key);
  final Task task;
  @override
  Widget build(BuildContext context) {
    return ListTile(
      key: ValueKey(task),
      title: Text(
        task.title,
        overflow: TextOverflow.ellipsis,
        style: TextStyle(
          decoration: task.isDone! ? TextDecoration.lineThrough : null),),
      trailing: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Checkbox(value: task.isDone, onChanged: (value) {
            context.read<TaskBloc>().add(UpdatedTask(task: task));
          }),
          IconButton(onPressed: () {
            _onDeleteTask(context, task);
          }, icon: const Icon(Icons.delete))
        ],
      ),
    );
  }

  void _onDeleteTask(BuildContext context, Task task) {
    task.isDeleted!
        ? context.read<TaskBloc>().add(DeletedTask(task: task))
        : context.read<TaskBloc>().add(RemovedTask(task: task));
  }
}



